<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Link -->a
    <link rel='stylesheet' type='text/css' media='screen' href='script/bootstrap.min.css'>
    <script src='script/jquery-3.2.1.slim.min.js'></script>
    <script src='script/popper.min.js'></script>
    <script src='script/bootstrap.min.js'></script>
    <script src="script/script.js"></script>
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.js"></script>
    <link rel="stylesheet" href="style/style.css">
    <link rel="icon" href="image/i1.png">
    <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
    <link href=”./output.css” rel=”stylesheet”>
    <!-- title webpage -->
    <!-- <script src="https://cdn.tailwindcss.com"></script> -->
    <title>Fast Food Website</title>
</head>

<body>
    <header class=" fixed top-0 bg-white h-12 w-screen">
        <div class="w-screen px-5 pt-3 pb-3 flex justify-between items-center bg-black h-12 text-white">
            <h4 class="text-white text-2xl"><ion-icon name="pizza-outline"></ion-icon> Fast <span class="font-bold">Food</span></h4>
            <div class="relative flex w-5/12 ">
                <input type="text" class="py-1 text-sm border-b border-white-500 bg-transparent w-full focus:outline-none focus:border-green-900 focus:transition-all" placeholder="Search products here....">
                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-4 h-6 absolute right-0">
                    <path stroke-linecap="round" stroke-linejoin="round" d="M21 21l-5.197-5.197m0 0A7.5 7.5 0 105.196 5.196a7.5 7.5 0 0010.607 10.607z" />
                  </svg>
                  
            </div>
            <div class="text-sm flex">
                <button type="button" class="border py-2 px-4 rounded-lg focus:outline-none">
                    Log in
                  </button>
                <button type="button" class="py-2 px-4 rounded-lg focus:outline-none">
                    Sign Up
                  </button>
                 
            </div>

        </div>
        <div class="w-full h-12 flex justify-center items-center bg-green-900">
           <ul class="flex justify-center items-center list-none text-white gap-8 decoration-0 ">
            <li><a class="rounded-full hover:font-semibold  no-underline hover:no-underline hover:text-green-900 hover:bg-white hover:rounded-lg p-1 px-3" href="">Home</a></li>
            <li><a class="rounded-full hover:font-semibold  no-underline hover:no-underline hover:text-green-900 hover:bg-white hover:rounded-lg p-1 px-3" href="">products</a></li>
            <li><a class="rounded-full hover:font-semibold  no-underline hover:no-underline hover:text-green-900 hover:bg-white hover:rounded-lg p-1 px-3" href="">Promotions</a></li>
            <li><a class="rounded-full hover:font-semibold  no-underline hover:no-underline hover:text-green-900 hover:bg-white hover:rounded-lg p-1 px-3" href="">About</a></li>
            <li><a class="rounded-full hover:font-semibold  no-underline hover:no-underline hover:text-green-900 hover:bg-white hover:rounded-lg p-1 px-3" href="">Location</a></li>
           </ul> 
        </div>

        <div id="carouselExampleIndicators" class="carousel slide bg-black" data-ride="carousel">
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
              <div class="carousel-item active w-full h-full">
                <img class="d-block w-100" src="image/rm1.png" alt="First slide">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="image/rm2.png" alt="Second slide">
              </div>
              <div class="carousel-item">
                <img class="d-block w-100" src="image/rm3.png" alt="Third slide">
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
       
    </header>
</body>

</html>